<?php
 
  // Medoo documentation: http://medoo.in/doc
  // include the medoo wrapper class
  require 'classes/medoo.php';
  
// create our $database object and connect to the database
  $database = new medoo([
  
    // required
    'database_type' => 'mysql',
    'database_name' => 'mp3Collection',
    'server' => 'localhost',
    'username' => 'DigitalDojo',
    'password' => 'ojoDlatigiD108',
    'charset' => 'utf8'
  ]);
  
/* -------------------------------------------------- */
 
  // get incoming POST values
  $queryType = $_POST['queryType'];
  $p1 = $_POST['p1'];


  if ($queryType === 'getAllTracks') {
    
    $songs = getAllTracks($database);
    
  } else if ($queryType === 'getAllTrackInfoForArtist') {
    
    $songs = getAllTrackInfoForArtist($database, $p1);
  } else if ($queryType === 'getAllTrackInfoForDate') {
    
    $songs = getAllTrackInfoForDate($database, $p1);
  } 

  // convert our multi-dimensional array to a JSON object 
  // and send it back to the browser
  echo json_encode($songs);

 




/* ------------------- functions ------------------------------- */


  // $songs is a multi-dimensional array of all the columns from the table 'tracks'
  // that have match our artist's name in the 'trackArtist' column

function getAllTracks($database) {
  
  $songs = $database->select('tracks', '*');

  return $songs;
}




function getAllTrackInfoForArtist($database, $p1) {
  
  // $songs is a multi-dimensional array of all the columns from the table 'tracks'
  // that have match our artist's name in the 'trackArtist' column
  $songs = $database->select('tracks', ['trackArtist', 'trackAlbumName', 'trackName'], ['trackArtist' => $p1]);
  
  return $songs;
}




function getAllTrackInfoForDate($database, $p1) {
  
  $songs = $database->select('tracks', ['trackArtist', 'trackAlbumName', 'trackName'], ['releaseDate[>=]' => $p1]);

  return $songs;
}

 
 

  