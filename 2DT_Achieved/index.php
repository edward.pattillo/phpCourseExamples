<!doctype html>
<html>

<head>
  <title>MP3 Collection</title>
  <link href='https://fonts.googleapis.com/css?family=Nixie+One' rel='stylesheet' type='text/css'>
  <link href='style.css' rel='stylesheet' type='text/css'>
  <!-- this gets the latest version of jQuery -->
  <script src="http://code.jquery.com/jquery-latest.min.js"></script>
  <script src="js.js"></script>
</head>

<body>

  <div id="menu">
    <button type="button" id="clearTracks">Clear</button>
    <button type="button" class="filterButton" id="Metallica">Metallica</button>
    <button type="button" class="filterButton" id="Edie Brickell & New Bohemians">Edie Brickell & New Bohemians</button>
    <button type="button" class="filterButton" id="Lenny Kravitz">Lenny Kravitz</button>
    <button type="button" class="filterButton" id="Marvin Gaye">Marvin Gaye</button>
    <button type="button" class="filterButton" id="Pink Floyd">Pink Floyd</button>
    <button type="button" class="filterButton" id="Bob Dylan">Bob Dylan</button>
    <button type="button" class="filterButton" id="Bob Marley & The Wailers">Bob Marley & The Wailers</button>
    <button type="button" class="filterButton" id="getAllTrackInfoForDate" name="1987-01-01">Since 1987</button>
    <button type="button" class="filterButton" id="getAllTracks">All</button>

  </div>
  
  <div id="trackArea"></div>

</body>

</html>