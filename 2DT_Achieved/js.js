
// our event listeners go inside the document ready function
$(document).ready(function() {
  
  // this event listener waits for someone to click 
  // on anything with a class 'filterButton'
  $('.filterButton').click(function() {
    
//     console.log(this.id);
    
    // initialise variables
    var queryType = '';
    var p1 = '';
    
    if (this.id == 'getAllTracks' ) {
      
      queryType = 'getAllTracks';
    } else if (this.id == 'getAllTrackInfoForDate' ) {
    
      queryType = 'getAllTrackInfoForDate';
      p1 = $('#'+this.id).attr('name');
    } else {
      
      queryType = 'getAllTrackInfoForArtist';
      p1 = this.id;
//       console.log(this.rel);
    }
    
    // send the specific id of the element that was click on
    // to a function called 'getTracks'
    getTracks(queryType, p1);
    
  }); // END OF $('.filterButton').click(function() { 
  
  
  // event listener to clear the contents of the trackArea
  $('#clearTracks').click(function() {

    $('#trackArea').html('');

  }); // END OF $('#clearTracks').click(function() {

}); // END OF $(document).ready(function() {


// ---------------------- functions -------------------------------

// this function makes an AJAX request that gets tracks by a particular artist
function getTracks(queryType, p1) {
  
  $.ajax({
    
    // specifices where the AJAX request will be sent to on the server
    url: "ajax.php",
    
    // an object with the data we want to send to the receiving file on the server
    data: { queryType: queryType, p1: p1 },
    
    // type of request to send (either GET or POST)
    type: "POST",
    
    // type of data to expect back from the server
    // The available data types are text, html, xml, json, jsonp, and script.
    dataType : 'json'
  
  })
  // .done defines what will happen when the ajax request is complete
  // we're going to name the response "returnData" and pass it to a function
  // that will present the track information
  .done(function(returnData) {
  
    $.each(returnData, function(key, trackInfo) {
      presentTrack(trackInfo);
    });
  })
  // .fail defines what will happen when the ajax request fails
  // we will have it present the error message returned from PHP at the top of the page
  .fail(function(returnData) {
    $('body').prepend(returnData.responseText);
  });
  
}



 
function presentTrack(trackInfo) {
  
  // build a string that contains the HTML for presenting our track
  var trackHTML = '<div class="track">';
    trackHTML += '<img src="albumArt/'+trackInfo.trackArtist+' - '+trackInfo.trackAlbumName+'.jpg" />';
    trackHTML += '<div class="trackInfo">';
      trackHTML += trackInfo.trackArtist+'<br>';
      trackHTML += trackInfo.trackName;
    trackHTML += '</div>';
  trackHTML += '</div>';
  
  // add this HTML as a child to the #trackArea div in our HTML
  $('#trackArea').append(trackHTML);
}
 
 
