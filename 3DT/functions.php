<?php




/* ------------------- functions ------------------------------- */



function dbConnect() {
  
// create our $database object and connect to the database
  $database = new medoo([
  
    // required
    'database_type' => 'mysql',
    'database_name' => 'mp3Collection',
    'server' => 'localhost',
    'username' => 'DigitalDojo',
    'password' => 'ojoDlatigiD108',
    'charset' => 'utf8'
  ]);

  return $database;
  
}


function deleteTrack($database, $trackID) {
  
  $database->delete('tracks', ['trackID' => $trackID]);
  $database->delete('albums_tracks', ['trackID' => $trackID]);
  $database->delete('artists_tracks', ['trackID' => $trackID]);
  
}


function updateInfo($database, $trackID, $artistInput, $albumInput, $trackInput) {
  
  $database->update('tracks', ['trackName' => $trackInput], ['trackID' => $trackID]);
  
}





function getSearchTracks($database, $searchString, $dateType, $searchDate) {
  
  $trackIDs = $database->select('tracks', ['trackID'], ['trackName' => $searchString]);
  
  $albumIDs = $database->select('albums', ['albumID'], ['albumName' => $searchString]);
  $newTrackIDs = getTrackIDsFromAlbumIDs($database, $albumIDs);
  $trackIDs = array_merge($trackIDs, $newTrackIDs);
  
  $artistIDs = $database->select('artists', ['artistID'], ['artistName' => $searchString]);
  $newTrackIDs = getTrackIDsFromArtistIDs($database, $artistIDs);
  $trackIDs = array_merge($trackIDs, $newTrackIDs);
  
  
  if ($dateType == 'Before' || $dateType == 'After') {
    
    $trackIDs = filterSearchResultsByDate($database, $trackIDs, $dateType, $searchDate);
  }
  
  return getAllTrackInfoFromTrackID($database, $trackIDs);
}




function filterSearchResultsByDate($database, $trackIDs, $dateType, $searchDate) {
  
  $filteredTracks = [];
  
  foreach ($trackIDs as $trackID) {
    
    $albums_tracks = $database->select('albums_tracks', ['albumID'], ['trackID' => $trackID]);
    $albumID = $albums_tracks[0]['albumID'];
    $albumInfo = $database->select('albums', ['albumReleaseDate'], ['albumID' => $albumID]);
    $albumReleaseDate = $albumInfo[0]['albumReleaseDate'];
  
    if ($dateType == 'Before' && strtotime($searchDate) > strtotime($albumReleaseDate)) {

      $filteredTracks[]['trackID'] = $trackID;
    } else if ($dateType == 'After' && strtotime($searchDate) <= strtotime($albumReleaseDate)) {

      $filteredTracks[]['trackID'] = $trackID;
    }
  }
  
  return $filteredTracks;
}






function getTrackIDsFromArtistIDs($database, $artistIDs) {
  
  $trackIDs = [];
  
  foreach($artistIDs as $artistID) {
    
    $artists_tracks = $database->select('artists_tracks', ['trackID'], ['artistID' => $artistID]);
    
    foreach($artists_tracks as $tracks) {
      
      $trackIDs[]['trackID'] = $tracks['trackID'];
    }
  }
  
  return $trackIDs;
}




function getTrackIDsFromAlbumIDs($database, $albumIDs) {
  
  $trackIDs = [];
  
  foreach($albumIDs as $albumID) {
    
    $albums_tracks = $database->select('albums_tracks', ['trackID'], ['albumID' => $albumID]);
    
    foreach($albums_tracks as $tracks) {
      
      $trackIDs[]['trackID'] = $tracks['trackID'];
    }
  }
  
  return $trackIDs;
}




 

function getAllTracks($database) {
  
  $trackIDs = $database->select('tracks', ['trackID']);
  
  return getAllTrackInfoFromTrackID($database, $trackIDs);
}




function getAllTrackInfoForArtist($database, $artistID) {
  
  $trackIDs = $database->select('artists_tracks', ['trackID'], ['artistID' => $artistID]);

  // get track details from our function
  return getAllTrackInfoFromTrackID($database, $trackIDs);
}


function getAllTrackInfoForDate($database, $testDate) {
  
  // get album ids where the date tests correctly
  $albumIDs = $database->select('albums', ['albumID'], ['albumReleaseDate[>=]' => $testDate]);
  
  // declare empty array to collect track ids
  $allTrackIds = [];
  
  foreach ($albumIDs as $albumID) {
    
    // get all the tracks for this particular album
    $trackIDs = $database->select('albums_tracks', ['trackID'], ['albumID' => $albumID]);
    // add the new array of track IDs with the old array
    $allTrackIds = array_merge($allTrackIds,$trackIDs);
  }
  
  return getAllTrackInfoFromTrackID($database, $allTrackIds);
}



function getAllTrackInfoFromTrackID($database, $trackIDs) {
    

  // empty array to hold track arrays
  $songs = [];
  
  foreach ($trackIDs as $track) {
 
    // declare and array that we will add to as we go 
    $trackDetails = [];
  
    $trackID = $track['trackID'];
    $trackDetails['trackID'] = $trackID;
      
      
    // get track info
    $trackInfo = $database->select('tracks', ['trackName'], ['trackID' => $trackID]);
    $trackDetails['trackName'] = $trackInfo[0]['trackName'];
  
 
    // get albumID so we can get album info
    $albums_tracks = $database->select('albums_tracks', ['albumID'], ['trackID' => $trackID]);
    $albumID = $albums_tracks[0]['albumID'];
  
    // get album info
    $albumInfo = $database->select('albums', ['albumName'], ['albumID' => $albumID]);
    $trackDetails['albumName'] = $albumInfo[0]['albumName'];

    // get artistID so we can get artist info
    $artists_tracks = $database->select('artists_tracks', ['artistID'], ['trackID' => $trackID]);
    $artistID = $artists_tracks[0]['artistID'];
   
    // get artist info
    $artistInfo = $database->select('artists', ['artistName'], ['artistID' => $artistID]);
    $trackDetails['artistName'] = $artistInfo[0]['artistName'];
    
    // add details to our song array (of arrays)
    $songs[] = $trackDetails;
  }
//     print_r($songs);
    return $songs;
}




// if the database call produced an error, we respond with the error message instead of a JSON object
function checkForDatabaseError($database,$songs) {
  
  // if $songs validates as false, something went wrong when we queried the database
  if ($songs === false) {
    
    // get the error details
    $errorArray = $database->error();
    
    // the 3rd value in the array contains the error message (starting with [0] )
    echo $errorArray[2].'<br>';
    
    // exit means to stop interpreting the PHP. 
    // nothing will be returned and anything after the line of code
    // where this function was called will execute  
    exit;
  }
}





/* 



  if ($queryType === 'getAllTracks') {
    
    $songs = getAllTracks($database);
    
  } else if ($queryType === 'getAllTrackInfoForArtist') {
    
    $songs = getAllTrackInfoForArtist($database, $p1);
  } else if ($queryType === 'getAllTrackInfoForDate') {
    
    $songs = getAllTrackInfoForDate($database, $p1);
  } else 
  
  
  
  */

  