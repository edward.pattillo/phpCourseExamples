
// we declare this at the top so that the scope is always available
var requestData = {'apiKey':'naylandDigitalTech2016'};



$(document).ready(function() {

	// var optionsCards = getAutoSuggestCardNames();
	// console.log(optionsCards);
	// setting up initial user interface state
	$("#getAllPrintings").prop("checked", true);
	$("#setNameSpace").hide();
		
 
// ------------------- Deckologist API Ajax functions -------------------------------------

// this first function is the one that requests the data from the API script you included
// in the head section of your HTML:  http://api.deckologist.com/deckologistAPI.js
  function useDeckologistAPI(requestData) {
 
	// the first thing it does is prepare the request object
	// i've made a function to do that below this one
 	requestData = prepareRequestData(requestData);
  
  //  this is here to show in the console what we're sending to the server
  	 console.log('requestData: '+ JSON.stringify(requestData)); 
 	
 	// send our request off to the API
    deckologistAPI(requestData).done(function(returnData) {
			
		// console.log(returnData);
		
		// send the returned card data to our checker function below
		checkCardreturnData(returnData);
	});		
  }
 
  	
  	
 // this function prepares the API request object by gathering all of the 
 // user input from the fields and checkboxes on the page
  function prepareRequestData(requestData) {
	
	 // get form input fields and create request     			
	requestData.requestedCardName =  $('#cardName').val();
	
	// look at checkboxes to determine the type of request
	if ($('#specificPrinting').prop('checked')) { 
		
		// if the user selects 'specific' we need to make sure they've
		// entered a set name
		if ($('#setName').val() != '') {
			requestData.request = 'specificPrinting'; 
			requestData.setName = $('#setName').val();
		} else {
			// if they haven't them set focus to the setName field and abort mission
			$('#setName').focus();
			return;	
		} 
		
		// if the user selects 'best price'
	} else if ($('#lowestPricePaper').prop('checked')) { 
		requestData.request = 'lowestPricePaper'; 
		
		// this will fall back to a full search for all printings of the card name entered
	} else { 
		// we default to getting all printings of a card name
		requestData.request = 'getAllPrintings'; 
	}
	// send our request object  back to the API function
	return requestData;
   	
  }
  

  	// this checks the object returned from the API 
  	function checkCardreturnData(returnData) {
  		
		// if our  request statement is broken the API will send us an object
		// with an error property that we can test for here		
		if (returnData.hasOwnProperty("error")) {
			
			// we're putting the error message into a <div id="output"> somewhere on the page
			// You can customise this. The error message is stored in returnData.error
			$("#output").html("<h2>" + returnData.error + "</h2>");

			
			// if the returned data has multiple printings we present it differently
		} else if (returnData.hasOwnProperty("multipleReturnValues")) {
		
			delete returnData.multipleReturnValues;
			showMultipleCards(returnData);

			
			// we fall back to a single card presentation if none of the criteria above are met
		} else {
			showSingleCard(returnData);
		}     	
	}
  	
  	

// ------------------- END OF Deckologist API Ajax functions -------------------------------------




  // ----------------------------------------------------------------------------------------
  // -------------------------------- autosuggest -------------------------------------------
  // ----------------------------------------------------------------------------------------

  
		// all card names for card field
		requestData.request = 'getAutoSuggestCardNames';
		var firstRequestFlag = true;
		var allCardNamesForAutoComplete = [];
		deckologistAPI(requestData).done(function(returnData) {
	
			var optionsCards = {
	
				data: JSON.parse(returnData.allCardNames) , 
				list: {
	
					onChooseEvent: function() {
						
						useDeckologistAPI(requestData);
		    		},
	
					match: {
						enabled: true
					},
					showAnimation: {
						type: "fade", //normal|slide|fade
						time: 400,
						callback: function() {}
					},
	
					hideAnimation: {
						type: "slide", //normal|slide|fade
						time: 400,
						callback: function() {}
					}
				}
			};
			
			if (firstRequestFlag) {
				$("#cardName").easyAutocomplete(optionsCards); 
				$("#cardName").focus();	
				firstRequestFlag = false;
			}
			allCardNamesForAutoComplete = optionsCards;
		});	   




	// set names for autosuggest (hidden by default)
	requestData.request = 'getAutoSuggestSetNames';
	
	deckologistAPI(requestData).done(function(returnData) {

		var optionsSets = {

			data: JSON.parse(returnData.allSetNames) , 
			list: {

				onChooseEvent: function() {
					
					// when the user selects a set we narrow down the 
					// autosuggest card names to limit them to only cards in that set
					getAutoSuggestCardNamesInSet($('#setName').val());
					$("#cardName").focus();
	    		},

				match: {
					enabled: true
				},
				showAnimation: {
					type: "fade", //normal|slide|fade
					time: 400,
					callback: function() {}
				},

				hideAnimation: {
					type: "slide", //normal|slide|fade
					time: 400,
					callback: function() {}
				}
			}
		};


		$("#setName").easyAutocomplete(optionsSets);

	});	 

	
	
	
  	// this allows us to narrow down the auto suggest for the card field
  	// when the user has selected a set so that only cards withing that set are shown
  	function getAutoSuggestCardNamesInSet(requestedSetName) {

		requestData.request = 'getAutoSuggestCardNamesInSet';
		requestData.setName = requestedSetName;
		deckologistAPI(requestData).done(function(returnData) {
	
			var optionsCardsInSet = {
	
				data: JSON.parse(returnData.cardNamesInSet) , 
				list: {
	
					onChooseEvent: function() {
						
						// broken at the moment
						requestData.request = 'specificPrinting';
						requestData.requestedCardName = $('#cardName').val();
						requestData.setName = $('#setName').val();
		      			returnData = useDeckologistAPI(requestData);
		    		},
	
					match: {
						enabled: true
					},
					showAnimation: {
						type: "fade", //normal|slide|fade
						time: 400,
						callback: function() {}
					},
	
					hideAnimation: {
						type: "slide", //normal|slide|fade
						time: 400,
						callback: function() {}
					}
				}
			};
			
			$("#cardName").easyAutocomplete(optionsCardsInSet);
			$("#cardName").focus();
		});	   		
 
  	}
 	 
	// ----------------------------------------------------------------------------------------
 	// -------------------------------- end of autosuggest ------------------------------------
 	// ----------------------------------------------------------------------------------------

 
 
 
 
 
 
 	// -------------------------------- event listeners ------------------------------------

	// show explanations of what each preset button does
	$(".checkButton").hover(function() {
	    $('#explanations').html(this.title);
	    }, function(){
	    $('#explanations').html('');
	}); 
 	
 	
 	// checkbox madness
	$(':checkbox').click(function(){

		var specificPrintingChecked = $('#specificPrinting').prop('checked');
	 	
		if (this.id == 'specificPrinting'){
			
			if (specificPrintingChecked) {
				$(':checkbox').each(function () { this.checked = false; });
				$(this).prop("checked","checked");
				// present set info
  				$( "#mainContainer" ).animate({ "top": "+=7vh" }, 400, function() { $("#setNameSpace").fadeIn(600); $("#setName").focus(); });
  				
			} else {
				// hide set info
				$("#setNameSpace").fadeOut(400, function() { $( "#mainContainer" ).animate({ "top": "-=7vh" }, "slow"); });
				$("#getAllPrintings").prop("checked", true);
				$("#setName").val('');
				requestData.setName = '';
				$("#cardName").easyAutocomplete(allCardNamesForAutoComplete);		
  				$("#cardName").focus();					
			}
		} else if ($(this).prop('checked')){
			
			if (specificPrintingChecked) {
				// hide set info
				$("#setNameSpace").fadeOut(400, function() { $( "#mainContainer" ).animate({ "top": "-=7vh" }, 400); });
				$("#setName").val('');
				requestData.setName = '';
				$("#cardName").easyAutocomplete(allCardNamesForAutoComplete);
  				$("#cardName").focus();				
			}
			
			$(':checkbox').each(function () { this.checked = false; });
			$(this).prop("checked","checked");

		} else {
			
			$(':checkbox').each(function () { this.checked = false; });
			$("#getAllPrintings").prop("checked", true);
		}
 
 	});
	
	
	// waits for user to click enter after entering a card name
	// to trigger an API call
	 $('#cardName').keypress(function(e) {
	   
	   var key = e.which;
	
	   if (key == 13) // the enter key code
	   {
			useDeckologistAPI(requestData);
	   }
	
	 });

   
  
  $('.ex').click(function() {

    var cName = $(this).text();
    // console.log(cName);
    $('#cardName').val(cName);
    $('#cardName').focus();
  });
 
}); // END OF $(document).ready(function() {


 
// -----------------------   present returned values -----------------------------

  	
  	function showSingleCard(returnData) {
  				 
  	 	// show what is coming back from the server
  	 	console.log('returnData: '+ JSON.stringify(returnData)); 
 
  				 
		var lastUpdated = new Date(returnData.lastUpdated * 1000);
		var price = parseFloat(returnData.price).toFixed(2);
		if (price == 'NaN') { price = ' (no price available)'; } 
		if (returnData.rarity == 'Mythic Rare') { returnData.rarity = 'mythic'; }
		if (returnData.rarity == 'Special') { returnData.rarity = 'mythic'; }
		
		var outputHTML = "<div class='cardTitle'>";
		outputHTML += "<i class='ss ss-" + returnData.setCode.toLowerCase() + " ss-1x ss-"+returnData.rarity.toLowerCase()+"'></i> "; // ss-grad 
		outputHTML +=  returnData.name;
		outputHTML += " <i class='ss ss-" + returnData.setCode.toLowerCase() + " ss-1x ss-"+returnData.rarity.toLowerCase()+"'></i>"; //  ss-grad
		outputHTML += "</div>";
		outputHTML += "<p>Price: US$" + price + "</p>";
		outputHTML += "<img class='cardImage' src='" + returnData.imageURL + "' />";
		outputHTML += "<span class='smallText'><p>Price Source: <a href='http://shop.tcgplayer.com/magic' target='_blank'>" + returnData.priceSource + ".com</a></p>";
		outputHTML += "Price Last Updated: " + lastUpdated + "</span>";
		
		$("#output").html(outputHTML);
		
		$('#cardName').val('');
		$('#cardName').focus();
  	}
  	
  	
  	function showMultipleCards(returnData) {



		if (returnData[0].rarity == 'Mythic Rare') { returnData[0].rarity = 'mythic'; }
		if (returnData[0].rarity == 'Special') { returnData[0].rarity = 'mythic'; }
		
		var outputHTML = "<div class='cardTitle'>";
		outputHTML += "<i class='ss ss-" + returnData[0].setCode.toLowerCase() + " ss-1x ss-"+returnData[0].rarity.toLowerCase()+"'></i> "; // ss-grad 
		outputHTML +=  returnData[0].name;
		outputHTML += " <i class='ss ss-" + returnData[0].setCode.toLowerCase() + " ss-1x ss-"+returnData[0].rarity.toLowerCase()+"'></i>"; //  ss-grad
		outputHTML += "</div>"; 
		outputHTML += "<span class='smallText'><p>Price Source: <a href='http://shop.tcgplayer.com/magic' target='_blank'>tcgplayer.com</a></p>";
		outputHTML += "Prices Last Updated: <span id='lu'></span></span>";
		$("#output").html(outputHTML);
		
		// start all over 
		outputHTML = '';
		var layoutFlag = 1;

		$.each(returnData, function (index, cardDetails) {
		
			if(!checkEven(layoutFlag)){ outputHTML += "<div class='multiCardArea'>"; } 
			
			var lastUpdated = new Date(cardDetails.lastUpdated * 1000);
			var price = parseFloat(cardDetails.price).toFixed(2);
			if (price == 'NaN') { price = ' (no price available)'; } else { $('#lu').html(lastUpdated); }
			
			if (cardDetails.rarity == 'Mythic Rare') { cardDetails.rarity = 'mythic'; }
			if (cardDetails.rarity == 'Special') { cardDetails.rarity = 'mythic'; }
			
   	 		// show what is coming back from the server
  	 		console.log('multipleReturnValues: '+ JSON.stringify(cardDetails)); 
 	 		
 	 		
 	 		
			outputHTML += "<div class='multiCardList'>";
				outputHTML += cardDetails.setName; 
				outputHTML += "<br><i class='ss ss-" + cardDetails.setCode.toLowerCase() + " ss-1x ss-"+returnData[0].rarity.toLowerCase()+"'></i> "; // ss-grad 
				outputHTML += "US$"+price; 
				outputHTML += "<div class='multiCardListImage'>";
					outputHTML += "<img class='cardImage' src='"+cardDetails.imageURL+"' />"; 
				outputHTML += "</div>"; 
			outputHTML += "</div>"; 
			if(checkEven(layoutFlag)){ outputHTML += "</div>";  }
			layoutFlag++;
 		});  	
		 

		$("#output").append(outputHTML);
		
		$('#cardName').val('');
		$('#cardName').focus(); 		
 			
  	}
  	
  	
  	
function checkEven(val){
     return (val%2 == 0);
}
