
// we declare this at the top so that the scope is always available
var requestData = {'apiKey':'naylandDigitalTech2016'};



$(document).ready(function() {
 


   $('#run').click(function(){
   	
		requestData.request = 'manual';
		requestData.table = 'cards';
		requestData.columns = 'multiverseid, setCode, manaCost, type';
		requestData.where = ['setCode|SOI'];
		
		  	
   		useDeckologistAPI(requestData);

   	});



 
// ------------------- Deckologist API Ajax functions -------------------------------------

// this first function is the one that requests the data from the API script you included
// in the head section of your HTML:  http://api.deckologist.com/deckologistAPI.js
  function useDeckologistAPI(requestData) {
  
  
  //  this is here to show in the console what we're sending to the server
  	 console.log('requestData: '+ JSON.stringify(requestData)); 
 	
 	// send our request off to the API
    deckologistAPI(requestData).done(function(returnData) {
			
		// console.log(returnData);
		
		// send the returned card data to our checker function below
		checkCardreturnData(returnData);
	});		
  }
 
  	 
  

  	// this checks the object returned from the API 
  	function checkCardreturnData(returnData) {
  		
		// if our  request statement is broken the API will send us an object
		// with an error property that we can test for here		
		if (returnData.hasOwnProperty("error")) {
			
			// we're putting the error message into a <div id="output"> somewhere on the page
			// You can customise this. The error message is stored in returnData.error
			$("#output").html("<h2>" + returnData.error + "</h2>");

			
			// if the returned data has multiple printings we present it differently
		} else if (returnData.hasOwnProperty("multipleReturnValues")) {
		
			delete returnData.multipleReturnValues;
			$("#output").html("<pre>" + JSON.stringify(returnData) + "</pre>");

			
			// we fall back to a single card presentation if none of the criteria above are met
		} else {
			$("#output").html("<pre>" + JSON.stringify(returnData) + "</pre>");
		}     	
	}
  	
  	

// ------------------- END OF Deckologist API Ajax functions -------------------------------------


   
    


}); //  END OF $(document).ready(function() {

 